import React, { useState } from "react";
 import './index.css';
function Number() {
  // Set the initial count state to zero, 0
  const [count, setCount] = useState(0);

  // Create handleIncrement event handler
  const handleIncrement = () => {
    setCount(prevCount => prevCount + 1);
  };

  //Create handleDecrement event handler
  const handleDecrement = () => {
    setCount(prevCount => prevCount - 1);
  };
  return (
    <div>
      <div>
        <button onClick={handleDecrement} className ="counter">-</button>
        <h5 className="counter">Gía trị {count}</h5>
        <button onClick={handleIncrement} className ="counter">+</button>
      </div>
      <button onClick={() => setCount(0)} className ="counter">Reset</button>
    </div>
  );
}

export default Number;